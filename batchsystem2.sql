/*
Navicat MySQL Data Transfer

Source Server         : imgan
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : batchsystem2

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-08-06 13:10:22
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `global_setting`
-- ----------------------------
DROP TABLE IF EXISTS `global_setting`;
CREATE TABLE `global_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key_object` varchar(30) DEFAULT NULL,
  `value_object` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UK_mbqytb0rb9amst48ynpmyejgx` (`key_object`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of global_setting
-- ----------------------------
INSERT INTO `global_setting` VALUES ('1', 'ip_hmi_webservice', '192.168.0.20:8081');

-- ----------------------------
-- Table structure for `planning`
-- ----------------------------
DROP TABLE IF EXISTS `planning`;
CREATE TABLE `planning` (
  `date` date NOT NULL,
  `plan_id` int(11) NOT NULL,
  `plan_no` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`date`,`plan_id`),
  KEY `FK_pddxgn51pq6wvc535g10a5uko` (`user_created_id`) USING BTREE,
  KEY `plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of planning
-- ----------------------------
INSERT INTO `planning` VALUES ('2018-05-09', '1', '14043', '2018-05-09 10:50:26', '1');
INSERT INTO `planning` VALUES ('2018-07-04', '0', null, '2018-07-19 14:42:35', '1');
INSERT INTO `planning` VALUES ('2018-08-03', '2', '14045', '2018-08-03 10:36:26', null);
INSERT INTO `planning` VALUES ('2018-08-04', '2', '14046', '2018-08-03 10:43:02', null);

-- ----------------------------
-- Table structure for `planning_list`
-- ----------------------------
DROP TABLE IF EXISTS `planning_list`;
CREATE TABLE `planning_list` (
  `plan_list_id` int(20) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT NULL,
  `batch_number` varchar(50) DEFAULT NULL,
  `batch_code` varchar(50) DEFAULT NULL,
  `qty_plan` int(100) DEFAULT NULL,
  `tank_id` bigint(20) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_running` bit(1) DEFAULT NULL,
  `recipe_id` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_created_id` bigint(20) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`plan_list_id`),
  UNIQUE KEY `indexPlan` (`plan_list_id`),
  KEY `recipePlan` (`recipe_id`) USING BTREE,
  KEY `tankPlan` (`tank_id`) USING BTREE,
  KEY `FK_plan` (`plan_id`),
  CONSTRAINT `FK_plan` FOREIGN KEY (`plan_id`) REFERENCES `planning` (`plan_id`),
  CONSTRAINT `FK_recipe` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  CONSTRAINT `FK_tank` FOREIGN KEY (`tank_id`) REFERENCES `tank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planning_list
-- ----------------------------
INSERT INTO `planning_list` VALUES ('1', '0', '', '', '500', '1', '2018-08-03 15:08:00', '2018-08-03 15:11:59', '', '25', '2018-08-03 15:11:59', '1', '0');
INSERT INTO `planning_list` VALUES ('2', '0', '', '', '500', '1', '2018-08-03 14:32:57', '2018-08-03 14:52:59', '', '25', '2018-08-03 14:52:59', '1', '0');
INSERT INTO `planning_list` VALUES ('3', '0', '', '', '600', '1', '2018-07-24 20:40:58', '2018-08-03 14:53:03', '', '25', '2018-08-03 14:53:03', '1', '0');
INSERT INTO `planning_list` VALUES ('4', '0', '', '', '500', null, '2018-07-24 20:40:58', '2018-08-03 14:53:06', '', '25', '2018-08-03 14:53:06', '1', '0');

-- ----------------------------
-- Table structure for `process`
-- ----------------------------
DROP TABLE IF EXISTS `process`;
CREATE TABLE `process` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `process_type` int(11) DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  `unit_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_85j0ex0uioglxjuyoiyniip02` (`user_created_id`) USING BTREE,
  KEY `FK_e0vtuymeqdnsdks05o1pgw1o0` (`user_updated_id`) USING BTREE,
  KEY `FK_4iob6pcenddivl9qjngpaurgq` (`unit_id`) USING BTREE,
  CONSTRAINT `FK_4iob6pcenddivl9qjngpaurgq` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`),
  CONSTRAINT `FK_85j0ex0uioglxjuyoiyniip02` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_e0vtuymeqdnsdks05o1pgw1o0` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of process
-- ----------------------------
INSERT INTO `process` VALUES ('1', '', '2018-05-09 10:47:21', '2018-05-09 10:47:21', 'EAP', '1', 'EAP', '1', '1', '1');
INSERT INTO `process` VALUES ('2', '', '2018-05-09 10:47:59', '2018-05-09 10:47:59', 'IPAP', '2', 'IPAP', '1', '1', '1');
INSERT INTO `process` VALUES ('3', '', '2018-05-09 10:48:44', '2018-05-09 10:48:44', 'AGIT', '3', 'AGIT', '1', '1', '1');
INSERT INTO `process` VALUES ('4', '', '2018-08-03 15:06:52', null, 'MEKP', '4', 'MEKP', '1', '1', '1');
INSERT INTO `process` VALUES ('5', '', '2018-08-03 15:06:57', null, 'TLP', '5', 'TLP', '1', '1', '1');
INSERT INTO `process` VALUES ('6', '', '2018-08-03 15:07:02', null, 'REAGIT', '6', 'Reagitation', '1', '1', '1');

-- ----------------------------
-- Table structure for `recipe`
-- ----------------------------
DROP TABLE IF EXISTS `recipe`;
CREATE TABLE `recipe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `activated_date` datetime DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `obsoluted_date` datetime DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `rejected_date` datetime DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  `user_activated_id` bigint(20) DEFAULT NULL,
  `user_approved_id` bigint(20) DEFAULT NULL,
  `user_obsoluted_id` bigint(20) DEFAULT NULL,
  `user_rejected_id` bigint(20) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_a5a3c8m9owqkw9phvjysgehns` (`user_created_id`) USING BTREE,
  KEY `FK_8w7b8i5tl9teggj5q93ys762g` (`user_updated_id`) USING BTREE,
  KEY `FK_ihomkn8we2ckko6tuyrbxi9m6` (`user_activated_id`) USING BTREE,
  KEY `FK_297delmsy2d0jw8tkmkq8pqy4` (`user_approved_id`) USING BTREE,
  KEY `FK_ihjidmtxb58vuikjc7hge5lvm` (`user_obsoluted_id`) USING BTREE,
  KEY `FK_9xyt3siao5okyagnypk69qv90` (`user_rejected_id`) USING BTREE,
  CONSTRAINT `FK_297delmsy2d0jw8tkmkq8pqy4` FOREIGN KEY (`user_approved_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_8w7b8i5tl9teggj5q93ys762g` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_9xyt3siao5okyagnypk69qv90` FOREIGN KEY (`user_rejected_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_a5a3c8m9owqkw9phvjysgehns` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ihjidmtxb58vuikjc7hge5lvm` FOREIGN KEY (`user_obsoluted_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ihomkn8we2ckko6tuyrbxi9m6` FOREIGN KEY (`user_activated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of recipe
-- ----------------------------
INSERT INTO `recipe` VALUES ('1', '', '2018-05-09 10:49:40', '2018-05-09 10:50:09', '2018-05-09 10:50:09', '2018-05-09 10:49:50', 'aa', '2018-06-23 04:09:38', '1000', null, 'aa1', '1', '1', '1', '1', '1', '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"2\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('7', '', '2018-06-23 04:03:52', null, '2018-06-23 04:04:59', '2018-06-23 04:04:45', 'ab', '2018-08-03 10:37:05', '500', null, 'ab1', '1', '1', null, null, null, '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"2\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('13', null, null, null, '2018-06-23 07:41:09', '2018-06-23 07:40:51', 'ah', '2018-06-23 07:41:18', '1000', null, 'ah', '1', null, null, '1', '1', '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"2\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('25', '', null, null, '2018-06-29 13:44:36', '2018-06-29 13:44:22', 'Planning007', '2018-08-03 10:37:11', '500', null, 'Planning007', '1', '1', null, '1', '1', '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"1\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"1\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"1\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"1\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"3\"}]}');
INSERT INTO `recipe` VALUES ('27', '', '2018-07-20 13:43:16', null, '2018-07-20 16:23:24', '2018-07-20 16:23:10', 'nafiul', '2018-08-03 10:37:14', '500', null, 'nma', '1', '1', null, '1', '1', '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"2\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('29', '', '2018-07-23 08:33:52', null, '2018-07-30 10:23:43', '2018-07-23 14:35:56', 'sadfadsafasd', '2018-08-03 10:37:17', '2134', null, 'ad', '1', '1', null, '1', '1', '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"2\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('33', '', '2018-07-31 09:29:25', null, '2018-07-31 09:29:34', '2018-07-31 09:29:29', 'Recipe10', null, '5000', null, 'Recipe10', '1', '1', null, '1', '1', null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"1\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"3\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"1\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"2\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"1\"}]}');
INSERT INTO `recipe` VALUES ('34', '', '2018-08-03 10:42:18', null, '2018-08-03 10:42:36', '2018-08-03 10:42:31', 'Recipe 9', null, '1200', null, '009', '1', '1', null, '1', '1', null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"1\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"1\"},{\"id_proses\":5,\"name\":\"TLP\",\"value\":\"3\"},{\"id_proses\":6,\"name\":\"REAGIT\",\"value\":\"1\"}]}');

-- ----------------------------
-- Table structure for `report`
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `date` date NOT NULL,
  `plan_id` int(11) NOT NULL,
  `plan_list_id` int(20) NOT NULL,
  `tank_id` bigint(20) NOT NULL,
  `process_id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `time` time NOT NULL,
  `pv` double DEFAULT NULL,
  PRIMARY KEY (`date`,`plan_id`,`tank_id`,`process_id`,`recipe_id`,`time`),
  KEY `FK_r7juuxjl0bxdycmso5ck19oqk` (`process_id`,`recipe_id`) USING BTREE,
  CONSTRAINT `FK_r7juuxjl0bxdycmso5ck19oqk` FOREIGN KEY (`process_id`) REFERENCES `process` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of report
-- ----------------------------
INSERT INTO `report` VALUES ('2018-07-30', '1', '1', '1', '1', '1', '01:48:23', '500');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_7m3eqg8noqk187mewgv2e5oee` (`user_created_id`) USING BTREE,
  KEY `FK_brt29v4kxb5kg964semh4t2vo` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_7m3eqg8noqk187mewgv2e5oee` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_brt29v4kxb5kg964semh4t2vo` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '', '2018-04-27 10:25:03', '2018-04-27 10:25:03', 'Administrator', 'Admin', null, null);
INSERT INTO `role` VALUES ('2', null, null, null, 'Users', 'User', null, null);
INSERT INTO `role` VALUES ('3', null, null, null, 'Manager', 'Manager', null, null);

-- ----------------------------
-- Table structure for `role_list_authority`
-- ----------------------------
DROP TABLE IF EXISTS `role_list_authority`;
CREATE TABLE `role_list_authority` (
  `role_id` bigint(20) NOT NULL,
  `list_authority` varchar(255) DEFAULT NULL,
  KEY `FK_5cbj9hyan2tu0bggh8v3t7620` (`role_id`) USING BTREE,
  CONSTRAINT `FK_5cbj9hyan2tu0bggh8v3t7620` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of role_list_authority
-- ----------------------------
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_USER_MANAGER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_MASTER_DATA');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_CREATOR');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_APPROVER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_ACTIVATOR');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_OBSOLUTER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_REVISE');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_PLANNER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RUNNER');

-- ----------------------------
-- Table structure for `step`
-- ----------------------------
DROP TABLE IF EXISTS `step`;
CREATE TABLE `step` (
  `recipe_id` bigint(20) NOT NULL,
  `step_id` int(11) NOT NULL,
  PRIMARY KEY (`recipe_id`,`step_id`) USING BTREE,
  CONSTRAINT `FK_bclpmti9g9mco7cua4yvus0q0` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of step
-- ----------------------------
INSERT INTO `step` VALUES ('1', '1');
INSERT INTO `step` VALUES ('1', '2');

-- ----------------------------
-- Table structure for `step_process`
-- ----------------------------
DROP TABLE IF EXISTS `step_process`;
CREATE TABLE `step_process` (
  `process_id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `step_id` int(11) NOT NULL,
  `next_step_condition` int(11) DEFAULT NULL,
  `next_step_condition_value` double DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `sp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`process_id`,`recipe_id`,`step_id`) USING BTREE,
  KEY `FK_gfmpti01svdfm65tk52mrl852` (`recipe_id`,`step_id`) USING BTREE,
  CONSTRAINT `FK_8o7lfd7o0sawpday2kj4iy0wo` FOREIGN KEY (`process_id`) REFERENCES `process` (`id`),
  CONSTRAINT `FK_gfmpti01svdfm65tk52mrl852` FOREIGN KEY (`recipe_id`, `step_id`) REFERENCES `step` (`recipe_id`, `step_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of step_process
-- ----------------------------
INSERT INTO `step_process` VALUES ('1', '1', '1', '1', '0', '0', '12.3');
INSERT INTO `step_process` VALUES ('2', '1', '1', '0', '0', '1', '1000');
INSERT INTO `step_process` VALUES ('3', '1', '2', '1', '0', '0', '65.7');

-- ----------------------------
-- Table structure for `tank`
-- ----------------------------
DROP TABLE IF EXISTS `tank`;
CREATE TABLE `tank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_7gt0rkvf99ksoj1sfv5ibon7s` (`user_created_id`) USING BTREE,
  KEY `FK_19lquh9mmm6cqhbgmqjevd7ys` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_19lquh9mmm6cqhbgmqjevd7ys` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_7gt0rkvf99ksoj1sfv5ibon7s` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tank
-- ----------------------------
INSERT INTO `tank` VALUES ('1', '', '2018-05-09 10:46:30', '2018-05-09 10:46:30', 'Tank 1', 'Tank1', '1', '1');
INSERT INTO `tank` VALUES ('2', null, null, null, 'Tank 2', 'Tank2', null, null);
INSERT INTO `tank` VALUES ('3', null, null, null, '', 'Tank3', null, null);
INSERT INTO `tank` VALUES ('4', null, null, null, '', 'Tank4', null, null);
INSERT INTO `tank` VALUES ('5', null, null, null, '', 'Tank5', null, null);
INSERT INTO `tank` VALUES ('6', null, null, null, '', 'Tank6', null, null);
INSERT INTO `tank` VALUES ('7', null, null, null, 'Tank Position B', 'Tank7', null, null);
INSERT INTO `tank` VALUES ('8', null, null, null, 'Tank Mixer bagian A', 'Tank8', null, null);

-- ----------------------------
-- Table structure for `unit`
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_hnnrudu0mkqbreyh20uft82fd` (`user_created_id`) USING BTREE,
  KEY `FK_2tx26n9kdoa5k97kd2yd4h0tu` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_2tx26n9kdoa5k97kd2yd4h0tu` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_hnnrudu0mkqbreyh20uft82fd` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES ('1', '', '2018-05-09 10:47:02', '2018-05-09 10:47:02', 'Kilo Gram', 'berat', 'kg', '1', '1');
INSERT INTO `unit` VALUES ('2', null, null, null, 'Liter', 'Cair', 'Ltr', null, null);
INSERT INTO `unit` VALUES ('3', null, null, null, 'Daftar Waktu', 'Waktu', 'dtk', null, null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_qleu8ddawkdltal07p8e6hgva` (`role_id`) USING BTREE,
  CONSTRAINT `FK_qleu8ddawkdltal07p8e6hgva` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com123', '1');
INSERT INTO `user` VALUES ('2', '', 'user', 'e10adc3949ba59abbe56e057f20f883e', 'Abulhamid@gmail.com', '1');
