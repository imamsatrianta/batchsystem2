var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
var md5 = require('md5');
/* GET Customer page. */

router.get('/',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * ,r.name as name_role FROM user u join role r on u.id = r.id ', function (err, rows) {
			if (err)
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);
			
			var  query2= connection.query('SELECT * FROM role', function(e, item){

				res.render('user/list', {
					title: "Users",
					data: rows,
					dataUnit : item
				});

			});
		});
         //console.log(query.sql);
     });
});

router.delete('/delete/(:id)',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var user = {
			id: req.params.id,
		}
		
		var delete_sql = 'delete from user where ?';
		req.getConnection(function(err,connection){
			var query = connection.query(delete_sql, user, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Delete : %s ",err);
					req.flash('msg_error', errors_detail); 
					res.redirect('/users');
				}
				else{
					req.flash('msg_info', 'Delete User Success'); 
					res.redirect('/users');
				}
			});
		});
	});
});
router.get('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * FROM user where id='+req.params.id,function(err,rows)
		{		
			if (err){
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);
			}
			var  query2= connection.query('SELECT * FROM role', function(e, item){

				res.render('user/edit', {
					title: "Users",
					data: rows,
					dataUnit : item
				});

			});

	});

});
});
		
	
router.put('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.assert('name', 'Please fill the name').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {
			v_name = req.sanitize( 'name' ).escape().trim(); 
			v_email = req.sanitize( 'email' ).escape().trim();
			v_role = req.sanitize( 'role' ).escape();
			v_password = req.sanitize('password').escape().trim(); 
			var user = {
				name: v_name,
				email: v_email,
				password: md5(v_password),
				role_id : v_role
			}
			if (v_password == null || v_password == ''){
				delete user.password;	
			} 

			console.log(user);
		var update_sql = 'update user SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, user, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('user/edit', 
					{ 
						name: req.param('name'), 
						email: req.param('email'),
						role_id: req.param('role_id'),
					});
				}else{
					req.flash('msg_info', 'Update User success'); 
					res.redirect('/users');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('user/add-user', 
		{ 
			name: req.param('name'), 
		});
	}
});

router.post('/',authentication_mdl.is_login, function(req, res, next) {
	req.assert('name', 'Please fill the name').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {

		v_name = req.sanitize( 'name' ).escape().trim(); 
		v_email = req.sanitize( 'email' ).escape().trim();
		v_active = 1;
		v_password = req.sanitize('password').escape().trim();
		v_role = req.sanitize( 'role' ).escape();

		var user = {
			name: v_name,
			email: v_email,
			active: v_active,
			password: md5(v_password),
			role_id : v_role
		}

		var insert_sql = 'INSERT INTO user SET ?';
		req.getConnection(function(err,connection){
			var query = connection.query(insert_sql, user, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Insert : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('user/add-user', 
					{ 
						name: req.param('name'), 
						email: req.param('email'),
						active: req.param('active'),
						password: req.param('password'),
						role: req.param('role_id'),
					});
				}else{
					req.flash('msg_info', 'Create User success'); 
					res.redirect('/users');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('user/add-user', 
		{ 
			name: req.param('name'), 
			address: req.param('address')
		});
	}

});

router.get('/add',authentication_mdl.is_login, function(req, res, next) {
	res.render(	'user/add-user', 
	{ 
		title: 'Add New User',
		name: '',
		email: '',
		phone:'',
		address:''
	});
});



module.exports = router;
