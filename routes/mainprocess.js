var express = require("express");
var router = express.Router();
var authentication_mdl = require("../middlewares/authentication");
var session_store;
/* GET Main process page. */

router.get("/", authentication_mdl.is_login, function (req, res, next) {
  req.getConnection(function (err, connection) {
    var query = connection.query(
      "SELECT process.*, unit.name AS name_unit FROM process LEFT JOIN unit ON process.unit_id = unit.id",
      function (err, rows) {
        if (err) var errornya = ("Error Selecting : %s ", err);
        req.flash("msg_error", errornya);

        var q = connection.query("SELECT * FROM unit", function (e, item) {
          res.render("mainprocess/list", {
            title: "Main Process",
            data: rows,
            dataUnit: item
          });
        });
      }
    );
    console.log(query.sql);
  });
});

router.delete("/delete/(:id)", authentication_mdl.is_login, function (
  req,
  res,
  next
) {
  req.getConnection(function (err, connection) {
    var process = {
      id: req.params.id
    };

    var delete_sql = "delete from process where ?";
    req.getConnection(function (err, connection) {
      var query = connection.query(delete_sql, process, function (err, result) {
        if (err) {
          var errors_detail = ("Error Delete : %s ", err);
          req.flash("msg_error", errors_detail);
          res.redirect("/mainprocess");
        } else {
          req.flash("msg_info", "Delete Main Process Success");
          res.redirect("/mainprocess");
        }
      });
    });
  });
});
router.get("/edit/(:id)", authentication_mdl.is_login, function (
  req,
  res,
  next
) {
  req.getConnection(function (err, connection) {
    var query = connection.query(
      "SELECT * FROM process where id=" + req.params.id,
      function (err, rows) {
        if (err) {
          var errornya = ("Error Selecting : %s ", err);
          req.flash("msg_error", errors_detail);
          res.redirect("/mainprocess");
        } else {
          if (rows.length <= 0) {
            req.flash("msg_error", "Main Process can't be find!");
            res.redirect("/mainprocess");
          } else {
            var q = connection.query("SELECT * FROM unit", function (e, item) {
              console.log(rows);
              res.render("mainprocess/edit", {
                title: "Edit Main Process ",
                data: rows[0],
                dataUnit: item
              });
            });
          }
        }
      }
    );
  });
});
router.put("/edit/(:id)", authentication_mdl.is_login, function (
  req,
  res,
  next
) {
  req.assert("name", "Please fill the Field").notEmpty();
  var errors = req.validationErrors();
  if (!errors) {
    v_process = req
      .sanitize("name")
      .escape()
      .trim();
    v_code = req
      .sanitize("unique_code")
      .escape()
      .trim();
    v_unit = req
      .sanitize("unit_id")
      .escape()
      .trim();

    var process = {
      name: v_process,
      unique_code: v_code,
      unit_id: v_unit
    };

    var update_sql = "update process SET ? where id = " + req.params.id;
    req.getConnection(function (err, connection) {
      var query = connection.query(update_sql, process, function (err, result) {
        console.log(err);
        if (err) {
          var errors_detail = ("Error Update : %s ", err);
          req.flash("msg_error", errors_detail);
          res.render("mainprocess/edit", {
            name: req.param("name"),
            unique_code: req.param("unique_code"),
            unit: req.param("unit")
          });
        } else {
          req.flash("msg_info", "Update Main Process success");
          res.redirect("/mainprocess");
        }
      });
    });
  } else {
    console.log(errors);
    errors_detail = "<p>Sory there are error</p><ul>";
    for (i in errors) {
      error = errors[i];
      errors_detail += "<li>" + error.msg + "</li>";
    }
    errors_detail += "</ul>";
    req.flash("msg_error", errors_detail);
    res.render("mainprocess", {
      name: req.param("name")
    });
  }
});

router.post("/", authentication_mdl.is_login, function (req, res, next) {
  req.assert("name", "Please fill the name").notEmpty();
  var errors = req.validationErrors();
  if (!errors) {
    v_process = req
      .sanitize("name")
      .escape()
      .trim();
    v_code = req
      .sanitize("unique_code")
      .escape()
      .trim();
    v_unit = req
      .sanitize("unit_id")
      .escape()
      .trim();
    // v_date = new Date.now();
    var process = {
      name: v_process,
      unique_code: v_code,
      unit_id: v_unit,
      // created_date: v_date
    };

    var insert_sql = "INSERT INTO process SET ?";
    var created_date = new Date()
      .toJSON()
      .slice(0, 10)
      .replace(/-/g, "/");
    // console.log(created_date)
    req.getConnection(function (err, connection) {
      var query = connection.query(insert_sql, process, function (err, result) {
        console.log(err);
        if (err) {
          var errors_detail = ("Error Insert : %s ", err);
          req.flash("msg_error", errors_detail);
          res.render("mainprocess/list", {
            name: req.param("name"),
            unique_code: req.param("unique_code"),
            unit: req.param("unit"),
            created_date: req.param("created_date")
          });
        } else {
          req.flash("msg_info", "Create Main Process success");
          res.redirect("/mainprocess");
        }
      });
    });
  } else {
    console.log(errors);
    errors_detail = "<p>Sory there are error</p><ul>";
    for (i in errors) {
      error = errors[i];
      errors_detail += "<li>" + error.msg + "</li>";
    }
    errors_detail += "</ul>";
    req.flash("msg_error", errors_detail);
    res.render("mainprocess", {
      process: req.param("mainprocess"),
      unique_code: req.param("unique_code")
    });
  }
});

router.get("/", authentication_mdl.is_login, function (req, res, next) {
  res.render("mainprocess", {
    title: "Add New Main Process"
  });
});

module.exports = router;