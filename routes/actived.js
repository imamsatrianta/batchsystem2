var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
/* GET Customer page. */

router.get('/',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * FROM recipe where obsoluted_date is null and approved_date is not null and activated_date is not null ',function(err,rows)
		{
			if(err)
				var errornya  = ("Error Selecting : %s ",err );   
			req.flash('msg_error', errornya);   
			res.render('actived/list',{title:"Actived",data:rows});
		});
         console.log(query.sql);
     });
});

router.delete('/delete/(:id)',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var tank = {
			id: req.params.id,
		}
		
		var delete_sql = 'delete from tank where ?';
		req.getConnection(function(err,connection){
			var query = connection.query(delete_sql, tank, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Delete : %s ",err);
					req.flash('msg_error', errors_detail); 
					res.redirect('/tanks');
				}
				else{
					req.flash('msg_info', 'Delete Tank Success'); 
					res.redirect('/tanks');
				}
			});
		});
	});
});
router.get('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * FROM tank where id='+req.params.id,function(err,rows)
		{
			if(err)
			{
				var errornya  = ("Error Selecting : %s ",err );  
				req.flash('msg_error', errors_detail); 
				res.redirect('/tanks'); 
			}else
			{
				if(rows.length <=0)
				{
					req.flash('msg_error', "Tank can't be find!"); 
					res.redirect('/tanks');
				}
				else
				{	
					console.log(rows);
					res.render('tank/edit',{title:"Edit Tank ",data:rows[0]});

				}
			}

		});
	});
});
router.put('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.assert('name', 'Please fill the Field').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {
		v_name = req.sanitize( 'name' ).escape().trim(); 
		v_description = req.sanitize( 'description' ).escape().trim();

		var tank = {
			name: v_name,
			description: v_description,
		}

		var update_sql = 'update tank SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, tank, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('tank/edit', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}else{
					req.flash('msg_info', 'Update User success'); 
					res.redirect('/tanks');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('tank/add-tank', 
		{ 
			name: req.param('name'), 
		});
	}
});

router.put('/revise/(:id)',authentication_mdl.is_login, function(req,res,next){
	var errors = req.validationErrors();
	if (!errors) {

		v_date = new Date();
		var active = {
			user_obsoluted_id: 1,
			obsoluted_date : v_date,
		}

		var update_sql = 'update recipe SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, active, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('actived/list', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}else{
					req.flash('msg_info', 'Recipe Obsoluted'); 
					res.redirect('/actived');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('actived/list', 
		{ 
			name: req.param('name'), 
		});
	}
});

router.post('/',authentication_mdl.is_login, function(req, res, next) {
	req.assert('name', 'Please fill the name').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {

		v_tank = req.sanitize( 'name' ).escape().trim(); 
		v_description = req.sanitize( 'des' ).escape().trim();


		var tank = {
			name: v_tank,
			description: v_description,

		}

		var insert_sql = 'INSERT INTO tank SET ?';
		req.getConnection(function(err,connection){
			var query = connection.query(insert_sql, tank, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Insert : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('tank/add-tank', 
					{ 
						tank: req.param('tank'), 
						description: req.param('description'),

					});
				}else{
					req.flash('msg_info', 'Create Tank success'); 
					res.redirect('/tanks');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('tank/add-tank', 
		{ 
			tank: req.param('tank'), 
			description: req.param('description')
		});
	}

});

router.get('/add',authentication_mdl.is_login, function(req, res, next) {
	res.render(	'tank/add-tank', 
	{ 
		title: 'Add New Tank',

	});
});



module.exports = router;
