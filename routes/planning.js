var express = require('express');  
var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
var session = require('express-session');
var engine = require('./engine');

/* GET recipe page. */


router.get('/',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT  * , r.name as recipe_name from planning_list p join recipe r on p.recipe_id = r.id where p.is_running = 0', function (err, plan_list) {
 
			if (err)
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);

			connection.query('SELECT * FROM tank', function(er, tank){
				if (er)
					var error2 = ("Error Selecting : %s ", er);
				req.flash('msg_error', error2);
					
				var query3 = connection.query('SELECT * '
							 +'FROM recipe where activated_date is not null '
							 +'and obsoluted_date is null', function(e, recipe){
							 res.render('planning/list', {
								title: "Planning",
								data: plan_list, 
								datatank: tank, 
								// untuk pemilihan modal
								dataUnit2 : recipe
							});
					});
				});  
			}); 

     });
});

/**
* Fungsi untuk edit  data planning
*/
router.put('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.assert('name', 'Please fill the Field').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {
		v_name = req.sanitize( 'name' ).escape().trim(); 
		v_description = req.sanitize( 'description' ).escape().trim();

		var tank = {
			name: v_name,
			description: v_description,
		}

		var update_sql = 'update tank SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, tank, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('tank/edit', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}else{
					req.flash('msg_info', 'Update User success'); 
					res.redirect('/tanks');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('tank/add-tank', 
		{ 
			name: req.param('name'), 
		});
	}
});

/**
* Fungsi untuk approve setelah pembuatan data planning
*/
router.put('/approve/(:id)',authentication_mdl.is_login, function(req,res,next){
	var errors = req.validationErrors();
	session_store = req.session;
	if (!errors) {
		v_approved_id = session_store.id = 1; 
		v_approved_date = new Date();

		var approved = {
			user_approved_id : v_approved_id,
			approved_date : v_approved_date,
		} 

		var update_sql = 'update recipe SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, approved, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('recipe/list', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}else{
					req.flash('msg_info', 'Approved'); 
					res.redirect('/planning');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('recipe/list', 
		{ 
			name: req.param('name'), 
		});
	}
});

/**
* Fungsi untuk menjalankan setelah pembuatan data planning
*/
router.put('/running/(:id)',authentication_mdl.is_login, function(req,res,next){
	var errors = req.validationErrors();
	session_store = req.session;

	console.log(session_store);
	if (!errors) {
		v_approved_id = session_store.id = 1; 
		v_start_date = new Date();
		var v_id_plan = req.params.id;

		var plan_run = {
			is_running : 1,
			start_date : v_start_date
		} 

		var update_sql = 'update planning_list SET ? where plan_list_id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, plan_run, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('planning/list', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}
				else
				{
					// menjalankan engine untuk running 
					engine.setDataBeijer(v_id_plan); 
					req.flash('msg_info', 'Machine Is Running'); 
					res.redirect('/planning');
				}		
			});
		});
	}
	else
	{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('planning/list', 
		{ 
			name: req.param('name'), 
		});
	}
});

/**
* Fungsi untuk mengambil data ketika akan melakukan edit planning
*/
router.get('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * FROM tank where id='+req.params.id,function(err,rows)
		{
			if(err)
			{
				var errornya  = ("Error Selecting : %s ",err );  
				req.flash('msg_error', errors_detail); 
				res.redirect('/tanks'); 
			}else
			{
				if(rows.length <=0)
				{
					req.flash('msg_error', "Tank can't be find!"); 
					res.redirect('/tanks');
				}
				else
				{	
					console.log(rows);
					res.render('tank/edit',{title:"Edit Tank ",data:rows[0]});

				}
			}

		});
	});
});

/**
* Fungsi untuk menyimpan data saat save planning
*/
router.post('/',authentication_mdl.is_login, function(req, res, next) {
	var errors = req.validationErrors();
	if (!errors) {
		date = new Date();
		v_date = req.sanitize( 'date' ).escape().trim();
		v_plan_no = req.sanitize('plan_no').escape().trim();
		v_recipe_id = req.sanitize('recipe_id').escape().trim();
		v_qty_plan = req.sanitize('qty_plan').escape().trim();
		
		v_user_created_id = 1;
		v_is_running = 0;
		

		req.getConnection(function(err, connection){
			connection.query('Select * from recipe where id = '+ v_recipe_id, function(err, recipe){
				req.getConnection(function(err, connection){
					connection.query('Select max(plan_id) as jem from planning', function(err, jembut){
						var but = jembut[0].jem+1;
					// console.log(but)
				// insert data planning
				var planning = {
					date : v_date,
					plan_id : but, 
					plan_no : v_plan_no, 
					created_date : date, 
				}
				var insert_sql = 'INSERT INTO planning SET ?';
				req.getConnection(function(err,connection){
					var query = connection.query(insert_sql, planning, function(err, result){
						if(err)
						{
							var errors_detail  = ("Error Insert : %s ",err );   
							req.flash('msg_error', errors_detail); 
							res.render('planning/list');
						}
						else
						{
							req.flash('msg_info', 'Create Planning success'); 
							res.redirect('/planning');
						}		
					});
				}); 


				var h_loop;
				// Quantity tank recipe
				var r_qty =  recipe[0].quantity;

				if(v_qty_plan % r_qty != 0)
				{
					// hasil sisa modulus
					var mod_qty = v_qty_plan % r_qty;
					var sisa_qty = v_qty_plan - mod_qty;

					// hasil jumlah loop
					h_loop = sisa_qty / r_qty; 

					for(var i = 1 ; i < h_loop; i++)
					{
						// batch number dan batch code
						let batch_c = 'PO_'+v_plan_no+'_'+i;
						let batch_n = 'PO_'+v_plan_no+'_'+i;


					 var planning_list = {
							plan_id : but,
							batch_code : batch_c,
							batch_number : batch_n,
							qty_plan : r_qty,
							tank_id : 1,
							start_date : date,
							is_running : v_is_running,
							recipe_id : v_recipe_id, 
							create_date :date,
							user_created_id: v_user_created_id,
							is_deleted : 0
						} 
						var plan_1 = 'INSERT INTO planning_list SET ?';
						req.getConnection(async function( err, connection){
							var query =await connection.query(plan_1, planning_list);
						}); 

					}

					// let batch_c = 'bn_'+v_plan_no+ '_'  + (h_loop+1);
					// let batch_n = 'bc_'+v_plan_no+ '_'  + (h_loop+1);


					// var planning_list_akhir = {
					// 	plan_id : but,
					// 	batch_code : batch_c,
					// 	batch_number : batch_n,
					// 	qty_plan : mod_qty,
					// 	tank_id : 1,
					// 	start_date : date,
					// 	is_running : v_is_running,
					// 	recipe_id : v_recipe_id, 
					// 	create_date :date,
					// 	user_created_id: v_user_created_id,
					// 	is_deleted : 0
					// } 
					// var plan_2 = 'INSERT INTO planning_list SET ?';
					// req.getConnection(async function( err, connection){
					// 	var query = await  connection.query(plan_1, planning_list_akhir);
					// }); 

					// memasukan nilai setelah hasil modulus hasil sisa bagi qty recipe

				}
				else
				{
					h_loop = v_qty_plan / r_qty;
					for(var i = 0 ; i < h_loop; !i++)
					{
						console.log(h_loop);
						
					}
				}
			});
		});
	});
		}); 
 
	}
	else
	{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('planning/list', 
		{ 
			// recipe: req.param('tank'), 
			// description: req.param('description')
		});
	}

});

router.get('/add',authentication_mdl.is_login, function(req, res, next) {
	res.render(	'tank/add-tank', 
	{ 
		title: 'Add New Tank',

	});
});


/*set auto change tank  in planning*/
router.post('/tank_change/(:id_tank)', function(req, res){
	req.getConnection(function(err, connection){

		let val = {
			tank_id : req.params.id_tank
		}

		let q = 'UPDATE planning_list SET ?';
		connection.query(q, val, function(err, result){ 
			if(err)
			{
				var errors_detail  = ("Error Delete : %s ",err);
				req.flash('msg_error', errors_detail); 
				res.redirect('/planning');
			}
			else
			{
				req.flash('msg_info', 'Update Tank ID Success'); 
				res.redirect('/planning');
			}
		});

	});
});


/*Untuk menghapus data*/
router.delete('/delete/(:id)',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var planning = {
			recipe_id: req.params.id,
		}
		
		var delete_sql = 'delete from planning where ?';
		req.getConnection(function(err,connection){
			var query = connection.query(delete_sql, planning, function(err, result){
				console.log(err)
				if(err)
				{
					var errors_detail  = ("Error Delete : %s ",err);
					req.flash('msg_error', errors_detail); 
					res.redirect('/planning');
				}
				else{
					req.flash('msg_info', 'Delete Planning Success'); 
					res.redirect('/planning');
				}
			});
		});
	});
});




module.exports = router;
