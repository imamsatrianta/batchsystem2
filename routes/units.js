var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
/* GET Unit page. */

router.get('/', authentication_mdl.is_login, function (req, res, next) {
	req.getConnection(function (err, connection) {
		var query = connection.query('SELECT * FROM unit', function (err, rows) {
			if (err)
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);
			res.render('unit/list', {
				title: "Units",
				data: rows
			});
		});
		console.log(query.sql);
	});
});

router.delete('/delete/(:id)', authentication_mdl.is_login, function (req, res, next) {
	req.getConnection(function (err, connection) {
		var unit = {
			id: req.params.id,
		}

		var delete_sql = 'delete from unit where ?';
		req.getConnection(function (err, connection) {
			var query = connection.query(delete_sql, unit, function (err, result) {
				if (err) {
					var errors_detail = ("Error Delete : %s ", err);
					req.flash('msg_error', errors_detail);
					res.redirect('/units');
				} else {
					req.flash('msg_info', 'Delete Unit Success');
					res.redirect('/units');
				}
			});
		});
	});
});
router.get('/edit/(:id)', authentication_mdl.is_login, function (req, res, next) {
	req.getConnection(function (err, connection) {
		var query = connection.query('SELECT * FROM unit where id=' + req.params.id, function (err, rows) {
			if (err) {
				var errornya = ("Error Selecting : %s ", err);
				req.flash('msg_error', errors_detail);
				res.redirect('/units');
			} else {
				if (rows.length <= 0) {
					req.flash('msg_error', "Unit can't be find!");
					res.redirect('/units');
				} else {
					console.log(rows);
					res.render('unit/edit', {
						title: "Edit Unit ",
						data: rows[0]
					});

				}
			}

		});
	});
});
router.put('/edit/(:id)', authentication_mdl.is_login, function (req, res, next) {
	req.assert('name', 'Please fill the Field').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {
		v_name = req.sanitize('name').escape().trim();
		v_code = req.sanitize('unique_code').escape().trim();
		v_description = req.sanitize('description').escape().trim();

		var unit = {
			name: v_name,
			unique_code: v_code,
			description: v_description,
		}

		var update_sql = 'update unit SET ? where id = ' + req.params.id;
		req.getConnection(function (err, connection) {
			var query = connection.query(update_sql, unit, function (err, result) {
				if (err) {
					var errors_detail = ("Error Update : %s ", err);
					req.flash('msg_error', errors_detail);
					res.render('unit/edit', {
						name: req.param('name'),
						description: req.param('description'),

					});
				} else {
					req.flash('msg_info', 'Update Unit success');
					res.redirect('/units');
				}
			});
		});
	} else {

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) {
			error = errors[i];
			errors_detail += '<li>' + error.msg + '</li>';
		}
		errors_detail += "</ul>";
		req.flash('msg_error', errors_detail);
		res.render('unit', {
			name: req.param('name'),
		});
	}
});

router.post('/', authentication_mdl.is_login, function (req, res, next) {
	req.assert('name', 'Please fill the name').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {

		v_unit = req.sanitize('name').escape().trim();
		v_code = req.sanitize('unique_code').escape().trim();
		v_description = req.sanitize('des').escape().trim();


		var unit = {
			name: v_unit,
			unique_code: v_code,
			description: v_description,

		}

		var insert_sql = 'INSERT INTO unit SET ?';
		req.getConnection(function (err, connection) {
			var query = connection.query(insert_sql, unit, function (err, result) {
				if (err) {
					var errors_detail = ("Error Insert : %s ", err);
					req.flash('msg_error', errors_detail);
					res.render('list', {
						name: req.param('name'),
						unique_code: req.param('unique_code'),
						description: req.param('description'),

					});
					console.log(insert_sql);
				} else {
					req.flash('msg_info', 'Create Unit success');
					res.redirect('/units');
				}
			});
		});
	} else {

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) {
			error = errors[i];
			errors_detail += '<li>' + error.msg + '</li>';
		}
		errors_detail += "</ul>";
		req.flash('msg_error', errors_detail);
		res.render('list', {
			unit: req.param('unit'),
			description: req.param('description')
		});
	}

});

router.get('/', authentication_mdl.is_login, function (req, res, next) {
	res.render('unit', {
		title: 'Add New Unit',

	});
});



module.exports = router;