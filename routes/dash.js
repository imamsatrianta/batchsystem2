var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;

/* GET home page. */
router.get('/',authentication_mdl.is_login, function(req, res, next) {
	res.render('main/dash',{title:"Dashboard"});
});

module.exports = router;