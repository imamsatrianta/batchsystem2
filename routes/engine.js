var express = require('express');  
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
var session = require('express-session'); 
var fs = require('fs');
var axios = require('axios');
const WebSocket = require('ws');

// data tags
var tag = require('./tags');

var mysql = require('mysql');
 
var connection = mysql.createConnection({
    host: 'localhost',
    user: 'root', // your mysql user
    password : '', // your mysql password
    port : 3306, //port mysql
    database:'batchsystem2' // your database name
});

// setting interval beijer dan alamat
const intervalScanBeijer = 1000;
let enableScanBeijer = true;

// let urlBeijer ='http://192.168.1.35:8089/tagBatch';
let urlBeijer ='http://192.168.0.20:8081/tagBatch';
// let urlBeijer ='http://192.168.0.20:8081/tagBatch';


// global.ip ;
// connection.query('SELECT * FROM global_setting where key_object = "ip_hmi_webservice" ', function(err, setting) {
//   if(err)
//     var errornya  = ("Error Selecting : %s ",err )
//   var ip_hmi = setting[0].value_object;
//   ip = (ip_hmi == null ) ? "http://192.168.1.35:8081" : ip_hmi;
// }); 


/* GET Customer page. */
// const ws = new WebSocket('ws://127.0.0.1:9999/wsnya');
const wss = new WebSocket.Server({ port: 9999 }); 
wss.broadcast = function broadcast(data) {
  console.log('send')
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) 
    {
      client.send(data);
    }
  });
};


wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(data) {
    // Broadcast to everyone else.
    wss.clients.forEach(function each(client) {
      if (client !== ws && client.readyState === WebSocket.OPEN && data !='init') 
      {
        client.send(data);
      }
      else
      {
        console.log('client ready')
      }
    });
  });
});


/** 
* function untuk membaca data cookies dari vbeijer agar data dapat stabil
* @return buffer cookies
*/
let dataCookies = function () {
  return new Promise((resolve, reject) => {
    fs.readFile('cookies.txt', function (err, buf) {
      if (err) {
        reject(err)
      }
      resolve( String(buf) )
    });
  });
}

/**
* Fungsi untuk melakuka set data tags
* @param = dataCookies, dataArrayVal = []
*/
let setDataTags = function ( dataCokkie, dataArrayVal) {
  return new Promise((resolve, reject) => {
      axios({
        url: urlBeijer,
        method: 'POST',
        data: {
          "setTags":dataArrayVal
        },
        headers: {
          Cookie: dataCokkie
        }
      }
    )
      .then( function (response) { 

        // menulis cookies terbaru apabila tidak sama
        let temp = response.headers["set-cookie"];
        if (!!temp)
        {
            let temp2 = temp[0].split(";", -1);

            dataCokkie = temp2[0];
            fs.writeFile('cookies.txt', dataCokkie, function(err, data){
              if (err) console.log(err);
              resolve()
            });
        }
        else
        {
          resolve()
        } 

      })
      .catch(function (error) {
        // handle error
        reject(String(error))
      })
  })
}

/**
* fungsi untuk menulis data ke beijer
* @param planning_list_id ;
* sample data = [
               {
                   name:  'tank1',
                   value:  111 
                },
                {
                   name:  'tank2',
                   value:  222
                }
            ]; 
*/ 
router.setDataBeijer = async function setValue( plan_list_id )
{
  try 
  {
      var d_tag =[];
      var q = 'SELECT p.*, t.name AS tank_name, r.value';
            q += ' FROM planning_list p'; 
            q += ' JOIN tank t ON p.tank_id = t.id'; 
            q += ' JOIN recipe r ON p.recipe_id = r.id'; 
            q += ' WHERE p.plan_list_id = '+plan_list_id;
            q += ' AND p.is_running = 1';


      connection.query( q ,async function(err, planning_list) {
        if(err)
          var errornya  = ("Error Selecting : %s ",err )
        for(var i = 0; i < planning_list.length; i++)
        {
          var iRecipe = JSON.parse( String(planning_list[i].value));
          var dRecipe = iRecipe.data; 
          var nTank = planning_list[i].tank_name.toLowerCase();
          var qtyPlan = planning_list[i].qty_plan;

          // mendapat pembagi sesuai perbandingan
          var pembagi = 0;
          for(var k = 0; k < dRecipe.length; k++)
          {
            pembagi += parseInt(dRecipe[k].value);
          }


          for(var j = 0; j < dRecipe.length; j++ )
          { 
            var nProses = dRecipe[j].name.toLowerCase();

            // menghitung perbandingan;
            var vProses = dRecipe[j].value; 

            var val = parseFloat(vProses / pembagi * qtyPlan);

            var set = nTank +"_"+ nProses +"_setpoin";  

            var tagValue = {
              name : set,
              value : parseFloat(val).toFixed(2)
              // value : val
            }

            d_tag.push(tagValue);  
          } 

        }

        console.log(d_tag);


        let dataCokkie  = await dataCookies(); 
        let res = await setDataTags( dataCokkie, d_tag );
      });  


  } 
  catch (error) 
  { 
      console.log(error);
  }
}

/**
* Fungsi untuk penyimpanan data ke database
*/
async function saveData(json_data, report){
  return new Promise((resolve, reject) => {
      var jData = json_data.tags;
      var dataReport = {};
      var save = true;
      var day = new Date();
      var time = day.getHours() + ":" + day.getMinutes() + ":" + day.getSeconds();

      // console.log(time);

      if(jData.length > 0)
      {

        for(var i = 0; i < report.length; i++)
        {


          for(var j = 0; j < jData.length; j++)
          { 
            let rNama = jData[j].name.toString();
            let rVal = jData[j].value;

            var nameTank = report[i].tank_name+'_done';  
            if( rNama === nameTank && rVal == 0)
            {
              if(save == true)
              {
                // menyimpan data
                dataReport = {
                  date : day,
                  plan_id : report[i].plan_id,
                  plan_list_id : report[i].plan_list_id,
                  tank_id : report[i].tank_id,
                  // process_id : report[i].process_id,
                  process_id : 1,
                  recipe_id : report[i].recipe_id,
                  time : time

                }

                console.log(dataReport);
                // menyimpan data ke database
                // var q = 'INSERT INTO report SET ?'
                // var a = connection.query(q, dataReport);
                // console.log(a.sql);
              }
              
            }

          }

        }
 
      }

  });
}


/**
* function untuk mengambil data tags
* @param = dataCookies , listTag = []
*/
let getDataBeijer = function (dataCokkie, listTag, report) {
  return new Promise((resolve, reject) => {
    axios({
        url: urlBeijer,
        method: 'POST',
        data: {
          "getTags":listTag
        },
        headers: {
          Cookie: dataCokkie
        }
      }
    )
      .then(function (response) { 
        // handle success
        try 
        { 

          let  dataSend = String(response.data).substring(1);
          var jsonData = JSON.parse(dataSend);

          // melakukan penyimpanan data pada database
          // console.log(jsonData); 
          saveData(jsonData, report);

          // websocket untuk kebutuhan data
          wss.clients.forEach( function each(client) {
            if (client.readyState === WebSocket.OPEN) 
            {
              client.send((jsonData));
            }
          });

        } 
        catch (error) 
        {
          console.log(error)
        }

        let temp = response.headers["set-cookie"]; 
        if (!!temp)
        {
          let temp2 = temp[0].split(";", -1);

          dataCokkie = temp2[0];
          fs.writeFile('cookies.txt', dataCokkie, function(err, data){
            if (err) console.log(err);
              resolve()
             });
        }
        else
        {
          resolve()
        }

      })
      .catch(function (error) {
        // handle error
        reject(String(error));
      })
  })
}

/**
* fungsi untuk looping data tags yang akan di ambil
* 
*/
async function getValue( listTags, report ) { 
  try 
  {
      let dataCokkie  = await dataCookies();
      let res = await getDataBeijer( dataCokkie, listTags, report ); 

      enableScanBeijer = true;
  } 
  catch (error) 
  {
      enableScanBeijer = true;
      console.log(error);
  }
}


/**
* Interval untuk pembacaan data tags dari beijer
* 
* let listTags = ['tank1', 'tank1_tag1']; 
*/
router.getDataBeijer = function() {

                        setInterval( () => {
                          var getTag =[], report = [];
                          var q = 'SELECT p.*, t.name AS tank_name, r.value';
                                q += ' FROM planning_list p'; 
                                q += ' JOIN tank t ON p.tank_id = t.id'; 
                                q += ' JOIN recipe r ON p.recipe_id = r.id'; 
                                q += ' WHERE p.is_running = 1';

                          connection.query( q ,async function(err, planning_list) {
                            if(err)
                              var errornya  = ("Error Selecting : %s ",err )

                            for(var i = 0; i < planning_list.length; i++)
                            {
                              var iRecipe = JSON.parse( String(planning_list[i].value));
                              var dRecipe = iRecipe.data;

                              var nTank = planning_list[i].tank_name.toLowerCase();
                              var qtyPlan = planning_list[i].qty_plan;

                              // mendapat pembagi sesuai perbandingan
                              var pembagi = 0;
                              for(var k = 0; k < dRecipe.length; k++)
                              {
                                pembagi += parseInt(dRecipe[k].value);
                              } 

                              // status dari tank
                              // var stsTank = nTank +'_status';
                              // getTag.push(stsTank);  
                              
                              // done dari tank
                              var dnTank = nTank +'_done';
                              getTag.push(dnTank);  
                              var stsTank = nTank +'_sts';
                              getTag.push(stsTank);  

                              var itemTag = [];
                              for(var j = 0; j < dRecipe.length; j++ )
                              {
                                var nProses = dRecipe[j].name.toLowerCase();
                                var stsProses = nTank +"_"+ nProses +"_status";  
                                getTag.push(stsProses);  
                                var point = nTank +"_"+ nProses +"_actual";  
                                getTag.push(point);
                                var set = nTank +"_"+ nProses +"_setpoin";
                                getTag.push(set);

                                var d = {
                                  proses_id : dRecipe[j].id_proses,
                                  tag : nTank +"_"+ nProses
                                }

                                itemTag.push(JSON.stringify(d));

                              } 
                              //insert data report 
                              let iReport = {
                                plan_id : planning_list[i].plan_id,
                                plan_list_id : planning_list[i].plan_list_id, 
                                tag : itemTag,
                                recipe_id : planning_list[i].recipe_id,
                                tank_id : planning_list[i].tank_id,
                                tank_name : nTank,
                                qty_plan : planning_list[i].qty_plan,
                              }

                              report.push(iReport);
                            }  

                            // console.log(getTag);
                            // mengambil data pada beijer
                            getValue( getTag, report ); 

                          });   
                        }, 6000); 

                      }

                      ///


let intervalScanBeijer2=4000

var listTagsG=[]
var responG;
for(let i=0;i<9;i++){
    // listTagsG.push('tank'+i+'_status')
    // listTagsG.push('tank'+i+'_done')
    listTagsG.push('tank'+i+'_eap'+'_actual')
    listTagsG.push('tank'+i+'_ipap'+'_actual')
    listTagsG.push('tank'+i+'_mekp'+'_actual')
    listTagsG.push('tank'+i+'_tlp'+'_actual')
}

let proses1 = function () {
  return new Promise((resolve, reject) => {
    fs.readFile('cookies.txt', function (err, buf) {
      if (err) {
        reject(err)
      }
      resolve(String(buf))
    });
  });
}
let proses2 = function (dataCokkie) {
  return new Promise((resolve, reject) => {
    axios({
      url: urlBeijer,
      method: 'POST',
      data: {
        "getTags":listTagsG
      },
      headers: {
        Cookie: dataCokkie
      }
    }
    )
      .then(function (response) {
        // handle success
        try {
         let  dataSend = String(response.data).replace("?","",-1)
        //   for (let index = 0; index < dataSend.tags.length; index++) {
        //     const element = dataSend.tags[index];
           
        //  }
        
        // console.log(dataSend)
        responG=dataSend

          // wss.broadcast(JSON.stringify(dataSend))
          // console.log(responG)
          // console.log(dataSend)
          responX = JSON.stringify(responG)
          responX = String(responG).replace("?","",-1)
          
          // console.log(responX)
          date = new Date();
          v_date = date;
          data={
              date:v_date,
              value:responX
          }
          var jem = 'INSERT INTO data SET ?'
          var but = connection.query(jem,data);
        } catch (error) {
          console.log(error)
        }
        let temp = response.headers["set-cookie"]
        // if()
        // SID=4cbe098f88;Expires=Wed, 03 Jul 2019 08:10:53 GMT;Path=/
        if (!!temp){
      let temp2 = temp[0].split(";", -1)
      // SID=4cbe098f88;
      // if (dataCokkie) {
        dataCokkie = temp2[0]
        fs.writeFile('cookies.txt', dataCokkie, function(err, data){
          if (err) console.log(err);
          resolve()
         });
        }else{
          resolve()
        }

      })
      .catch(function (error) {
        // handle error
        reject(String(error))
      })
  })
}

async function loop() {
  try {
      let dataCokkie  = await proses1()
    // console.log(dataCokkie)
      let res = await proses2(dataCokkie)
    enableScanBeijer=true
    // wss.broadcast(res)
  } catch (error) {
    enableScanBeijer=true
    console.log(error)
  }
}

setInterval(()=>{
  if (enableScanBeijer==true){
    enableScanBeijer=false
    loop()
  }
},intervalScanBeijer2)


router.get('/tes123',function(req,res){
  res.send(responG)
})



module.exports = router;
