var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
var session = require('express-session');
/* GET recipe page. */
router.get('/',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * from recipe where approved_date is null and activated_date is null and obsoluted_date is null  ', function (err, rows) {
			if (err)
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);
			
			var query2 = connection.query('SELECT * FROM tank', function(e, tank){

			var query3 =	connection.query('SELECT * FROM process', function(e, proces){

			
						 res.render('recipe/list', {
							title: "recipe",
						data: rows,
						dataUnit : tank,
						dataUnit2 : proces
						});
				});
			});
		});
         console.log(query.sql);
     });
});


router.put('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.assert('name', 'Please fill the Field').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {
		v_name = req.sanitize( 'name' ).escape().trim(); 
		v_description = req.sanitize( 'description' ).escape().trim();

		var tank = {
			name: v_name,
			description: v_description,
		}

		var update_sql = 'update tank SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, tank, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('tank/edit', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}else{
					req.flash('msg_info', 'Update User success'); 
					res.redirect('/tanks');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('tank/add-tank', 
		{ 
			name: req.param('name'), 
		});
	}
});


router.put('/approve/(:id)',authentication_mdl.is_login, function(req,res,next){
	var errors = req.validationErrors();
	session_store = req.session;
	if (!errors) {
		v_approved_id = session_store.id = 1; 
		v_approved_date = new Date();

		var approved = {
			user_approved_id : v_approved_id,
			approved_date : v_approved_date,
			rejected_date: null,
			user_rejected_id :null
		}
		console.log(Date.now());
		var update_sql = 'update recipe SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, approved, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('recipe/list', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}else{
					req.flash('msg_info', 'Approved'); 
					res.redirect('/recipe');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('recipe/list', 
		{ 
			name: req.param('name'), 
		});
	}
});


router.delete('/delete/(:id)',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var recipe = {
			id: req.params.id,
		}
		
		var delete_sql = 'delete from recipe where ?';
		req.getConnection(function(err,connection){
			var query = connection.query(delete_sql, recipe, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Delete : %s ",err);
					req.flash('msg_error', errors_detail); 
					res.redirect('/recipe');
				}
				else{
					req.flash('msg_info', 'Delete Recipe Success'); 
					res.redirect('/recipe');
				}
			});
		});
	});
});

/**
* Fungsi untuk mengambil data recipe yang akan diedit
*/
router.get('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * FROM recipe where id=' + req.params.id,function(err,rows)
		{		
			if (err){
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);
			}
			var  query2= connection.query('SELECT * FROM process', function(e, item){

				console.log(item);
				res.render('recipe/edit', {
					title: "Recipe",
					data: rows,
					dataUnit : item
				});

			});

		});
	});
});

/**
* Hasil simpan edit 
*/
router.put('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	req.assert('name', 'Please fill the Field').notEmpty();
	var errors = req.validationErrors();
	if (!errors) {
		v_name = req.sanitize( 'name' ).escape().trim(); 
		v_description = req.sanitize( 'description' ).escape().trim();

		var tank = {
			name: v_name,
			description: v_description,
		}

		var update_sql = 'update tank SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, tank, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('tank/edit', 
					{ 
						name: req.param('name'), 
						description: req.param('description'),
						
					});
				}else{
					req.flash('msg_info', 'Update User success'); 
					res.redirect('/tanks');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('tank/add-tank', 
		{ 
			name: req.param('name'), 
		});
	}
});


/**
* Fungsi untuk penyimpanan data recipe dari modal pada tahap
*/
router.post('/',authentication_mdl.is_login, function(req, res, next) {
	var errors = req.validationErrors();
	
	if (!errors) { 
		date = new Date();

		v_name = req.sanitize( 'name' ).escape().trim(); 
		v_unique_code = req.sanitize( 'unique_code' ).escape().trim();
		v_quantity = req.sanitize( 'quantity' ).escape().trim();
		v_version = 1; 
		v_id_main = JSON.parse("["+ req.sanitize('id_main_p').escape().trim() + "]"); 
		v_perbandingan = req.sanitize('pb').escape().trim();   
		v_name_main = req.sanitize('name_main').escape().trim().split(',');  

		v_date = date;
		v_perbandingan = v_perbandingan.split(','); 
		
		
		var a_pb = []; var a_sp = [];
		for(var i = 0; i < v_id_main.length; i++)
		{
			if(v_perbandingan[i] !== '')
			{ 
				var d_pb = {
					id_proses : v_id_main[i],
					name : v_name_main[i],
					value : v_perbandingan[i]
				};

				a_pb.push(d_pb);
			} 

		} 

		var j_pb ;
		if( a_pb.length > 0)
		{
			j_pb = {
				data : a_pb
			}
			j_pb = JSON.stringify(j_pb);
		}
		else
		{
			j_pb = null;
		}

		v_status = 1;
		

		var recipe = {
			name: v_name,
			quantity: v_quantity,
			unique_code:v_unique_code,
			version:v_version,
			user_created_id:1,
			active:v_status,
			value : j_pb, 
			created_date : v_date,

		}

		var insert_sql = 'INSERT INTO recipe SET ?'; 
		req.getConnection(function(err,connection){
			var query = connection.query(insert_sql, recipe, function(err, result){
				if(err)
				{ 	
					req.flash('msg_info', 'Create Recipe Failed'); 
					res.redirect('/recipe');
				}
				else
				{
					req.flash('msg_info', 'Create Recipe success'); 
					res.redirect('/recipe');
				}		
			});
		});
	}
	else
	{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('recipe/list', 
		{ 
			// recipe: req.param('tank'), 
			// description: req.param('description')
		});
	}

});

router.get('/add',authentication_mdl.is_login, function(req, res, next) {
	res.render(	'tank/add-tank', 
	{ 
		title: 'Add New Tank',

	});
});



module.exports = router;
