var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store; 

router.dataTags = function(tank, proces){
	var d_tag = [];
	if(tank.length > 0 )
	{
		for(var i = 0; i < tank.length; i++)
		{
			// list untuk tank dimasukan kedalam return data
			var ns = tank[i].name + "_status";
			d_tag.push(ns.toLowerCase()); 

			var nd = tank[i].name + "_done";
			d_tag.push(nd.toLowerCase()); 

			for(var j = 0; j < proces.length; j++)
			{
				var ps = tank[i].name +"_"+ proces[j].name +"_setpoin";
				d_tag.push(ps.toLowerCase());
				var pp = tank[i].name +"_"+ proces[j].name + "_pointvalue";
				d_tag.push(pp.toLowerCase());
				var pst = tank[i].name +"_"+ proces[j].name + "_status";
				d_tag.push(pst.toLowerCase()); 
			}
		}
		
	} 

	return d_tag;

} 

router.get('/',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT * FROM tank',function(err,tank)
		{
			if(err)
				var errornya  = ("Error Selecting : %s ",err );

			 var q = connection.query('SELECT * FROM process', function(e, proces){

			 	var iTag = router.dataTags(tank, proces);  
			   //data tags yang di gunakan untuk  
				req.flash('msg_error', errornya);   
				res.render('tags/list',{title:"Tags", data:iTag});
			 	
			 })
		}); 
     });
}); 
  
module.exports = router;
