var express = require('express');  
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
var jadepdf = require('jade-pdf-redline')
var fs = require('fs')
var session = require('express-session');
var request = require('request');
/* GET recipe page. */
router.get('/',authentication_mdl.is_login, function(req, res, next) {
	req.getConnection(function(err,connection){
		var query = connection.query('SELECT  * from report', function (err, rows) {
			if (err)
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);
					    res.render('history/list', {
						title: "History",
						data: rows,
				
						});
				
			
		});
         console.log(query.sql);
     });
});
router.post('/excel', function(req,res){	
    var data = {
        template: {
            'shortid':'r18kyD83ce'
        }

    }
    var options = {
        uri :'http://localhost:8001/api/report',
        method: 'POST',
        json: data
    }
    request(options).pipe(res);
});

// router.post('/pdf', function(req,res){
//     req.getConnection(function(err,connection){	
//     var data = {

//         template: {
//             'shortid':'HyOkY_N4X'
//         },
//         options:{
            
//         }

//     }
//     var options = {
//         uri :'http://localhost:8001/api/report',
//         method: 'POST',
//         json: data
//     }
//     request(options).pipe(res);
//     });
// });
router.post('/pdf', function(req,res){
    req.getConnection(function(err,connection){	
        connection.query('select * from report',function(err,result){
            fs.createReadStream('./views/report/pdf.jade')
            .pipe(jadepdf({paperFormat:'A4',locals:{
                data:result }}))
           .pipe(res);    
        })   
    })   
});

module.exports = router;
