var express = require('express');
var router = express.Router();
var authentication_mdl = require('../middlewares/authentication');
var session_store;
/* GET Unit page. */

router.get('/', authentication_mdl.is_login, function (req, res, next) {
	req.getConnection(function (err, connection) {
		var query = connection.query('SELECT * from global_setting', function (err, rows) {
			if (err)
				var errornya = ("Error Selecting : %s ", err);
			req.flash('msg_error', errornya);
			res.render('setting/list', {
				title: "HMI IP",
				data: rows
			});
		});
		console.log(query.sql);
	});
});


router.get('/edit/(:id)', authentication_mdl.is_login, function (req, res, next) {
	req.getConnection(function (err, connection) {
		var query = connection.query('SELECT * FROM global_setting where id=' + req.params.id, function (err, rows) {
			if (err) {
				var errornya = ("Error Selecting : %s ", err);
				req.flash('msg_error', errors_detail);
				res.redirect('/setting');
			} else {
				if (rows.length <= 0) {
					req.flash('msg_error', "Unit can't be find!");
					res.redirect('/setting');
				} else {
					console.log(rows);
					res.render('setting/edit', {
						title: "Edit Unit ",
						data: rows[0]
					});

				}
			}

		});
	});
});

router.put('/edit/(:id)',authentication_mdl.is_login, function(req,res,next){
	var errors = req.validationErrors();
	if (!errors) {
		v_ip = req.sanitize( 'value_object' ).escape().trim(); 
		var g_setting = {
			value_object: v_ip,
		}

		var update_sql = 'update global_setting SET ? where id = '+req.params.id;
		req.getConnection(function(err,connection){
			var query = connection.query(update_sql, g_setting, function(err, result){
				if(err)
				{
					var errors_detail  = ("Error Update : %s ",err );   
					req.flash('msg_error', errors_detail); 
					res.render('setting/edit', 
					{ 
	
					});
				}else{
					req.flash('msg_info', 'Update IP HMI success'); 
					res.redirect('/setting');
				}		
			});
		});
	}else{

		console.log(errors);
		errors_detail = "<p>Sory there are error</p><ul>";
		for (i in errors) 
		{ 
			error = errors[i]; 
			errors_detail += '<li>'+error.msg+'</li>'; 
		} 
		errors_detail += "</ul>"; 
		req.flash('msg_error', errors_detail); 
		res.render('setting/list', 
		{ 
			name: req.param('name'), 
		});
	}
});


module.exports = router;