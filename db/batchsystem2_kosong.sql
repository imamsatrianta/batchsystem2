/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 100131
Source Host           : localhost:3306
Source Database       : batchsystem2

Target Server Type    : MYSQL
Target Server Version : 100131
File Encoding         : 65001

Date: 2018-08-02 10:47:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for global_setting
-- ----------------------------
DROP TABLE IF EXISTS `global_setting`;
CREATE TABLE `global_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key_object` varchar(30) DEFAULT NULL,
  `value_object` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UK_mbqytb0rb9amst48ynpmyejgx` (`key_object`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of global_setting
-- ----------------------------
INSERT INTO `global_setting` VALUES ('1', 'ip_hmi_webservice', '192.168.1.35:8089');

-- ----------------------------
-- Table structure for planning
-- ----------------------------
DROP TABLE IF EXISTS `planning`;
CREATE TABLE `planning` (
  `date` date NOT NULL,
  `plan_id` int(11) NOT NULL,
  `plan_no` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`date`,`plan_id`),
  KEY `FK_pddxgn51pq6wvc535g10a5uko` (`user_created_id`) USING BTREE,
  KEY `plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of planning
-- ----------------------------

-- ----------------------------
-- Table structure for planning_list
-- ----------------------------
DROP TABLE IF EXISTS `planning_list`;
CREATE TABLE `planning_list` (
  `plan_list_id` int(20) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT NULL,
  `batch_number` varchar(50) DEFAULT NULL,
  `batch_code` varchar(50) DEFAULT NULL,
  `qty_plan` int(100) DEFAULT NULL,
  `tank_id` bigint(20) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_running` bit(1) DEFAULT NULL,
  `recipe_id` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_created_id` bigint(20) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`plan_list_id`),
  UNIQUE KEY `indexPlan` (`plan_list_id`),
  KEY `recipePlan` (`recipe_id`) USING BTREE,
  KEY `tankPlan` (`tank_id`) USING BTREE,
  KEY `FK_plan` (`plan_id`),
  CONSTRAINT `FK_plan` FOREIGN KEY (`plan_id`) REFERENCES `planning` (`plan_id`),
  CONSTRAINT `FK_recipe` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  CONSTRAINT `FK_tank` FOREIGN KEY (`tank_id`) REFERENCES `tank` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planning_list
-- ----------------------------

-- ----------------------------
-- Table structure for process
-- ----------------------------
DROP TABLE IF EXISTS `process`;
CREATE TABLE `process` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `process_type` int(11) DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  `unit_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_85j0ex0uioglxjuyoiyniip02` (`user_created_id`) USING BTREE,
  KEY `FK_e0vtuymeqdnsdks05o1pgw1o0` (`user_updated_id`) USING BTREE,
  KEY `FK_4iob6pcenddivl9qjngpaurgq` (`unit_id`) USING BTREE,
  CONSTRAINT `FK_4iob6pcenddivl9qjngpaurgq` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`),
  CONSTRAINT `FK_85j0ex0uioglxjuyoiyniip02` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_e0vtuymeqdnsdks05o1pgw1o0` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of process
-- ----------------------------

-- ----------------------------
-- Table structure for recipe
-- ----------------------------
DROP TABLE IF EXISTS `recipe`;
CREATE TABLE `recipe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `activated_date` datetime DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `obsoluted_date` datetime DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `rejected_date` datetime DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  `user_activated_id` bigint(20) DEFAULT NULL,
  `user_approved_id` bigint(20) DEFAULT NULL,
  `user_obsoluted_id` bigint(20) DEFAULT NULL,
  `user_rejected_id` bigint(20) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_a5a3c8m9owqkw9phvjysgehns` (`user_created_id`) USING BTREE,
  KEY `FK_8w7b8i5tl9teggj5q93ys762g` (`user_updated_id`) USING BTREE,
  KEY `FK_ihomkn8we2ckko6tuyrbxi9m6` (`user_activated_id`) USING BTREE,
  KEY `FK_297delmsy2d0jw8tkmkq8pqy4` (`user_approved_id`) USING BTREE,
  KEY `FK_ihjidmtxb58vuikjc7hge5lvm` (`user_obsoluted_id`) USING BTREE,
  KEY `FK_9xyt3siao5okyagnypk69qv90` (`user_rejected_id`) USING BTREE,
  CONSTRAINT `FK_297delmsy2d0jw8tkmkq8pqy4` FOREIGN KEY (`user_approved_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_8w7b8i5tl9teggj5q93ys762g` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_9xyt3siao5okyagnypk69qv90` FOREIGN KEY (`user_rejected_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_a5a3c8m9owqkw9phvjysgehns` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ihjidmtxb58vuikjc7hge5lvm` FOREIGN KEY (`user_obsoluted_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ihomkn8we2ckko6tuyrbxi9m6` FOREIGN KEY (`user_activated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of recipe
-- ----------------------------

-- ----------------------------
-- Table structure for report
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `date` date NOT NULL,
  `plan_id` int(11) NOT NULL,
  `plan_list_id` int(20) NOT NULL,
  `tank_id` bigint(20) NOT NULL,
  `process_id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `time` time NOT NULL,
  `pv` double DEFAULT NULL,
  PRIMARY KEY (`date`,`plan_id`,`tank_id`,`process_id`,`recipe_id`,`time`),
  KEY `FK_r7juuxjl0bxdycmso5ck19oqk` (`process_id`,`recipe_id`) USING BTREE,
  CONSTRAINT `FK_r7juuxjl0bxdycmso5ck19oqk` FOREIGN KEY (`process_id`) REFERENCES `process` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of report
-- ----------------------------

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_7m3eqg8noqk187mewgv2e5oee` (`user_created_id`) USING BTREE,
  KEY `FK_brt29v4kxb5kg964semh4t2vo` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_7m3eqg8noqk187mewgv2e5oee` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_brt29v4kxb5kg964semh4t2vo` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '', '2018-04-27 10:25:03', '2018-04-27 10:25:03', 'Administrator', 'Admin', null, null);
INSERT INTO `role` VALUES ('2', null, null, null, 'Users', 'User', null, null);
INSERT INTO `role` VALUES ('3', null, null, null, 'Manager', 'Manager', null, null);

-- ----------------------------
-- Table structure for role_list_authority
-- ----------------------------
DROP TABLE IF EXISTS `role_list_authority`;
CREATE TABLE `role_list_authority` (
  `role_id` bigint(20) NOT NULL,
  `list_authority` varchar(255) DEFAULT NULL,
  KEY `FK_5cbj9hyan2tu0bggh8v3t7620` (`role_id`) USING BTREE,
  CONSTRAINT `FK_5cbj9hyan2tu0bggh8v3t7620` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of role_list_authority
-- ----------------------------
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_USER_MANAGER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_MASTER_DATA');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_CREATOR');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_APPROVER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_ACTIVATOR');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_OBSOLUTER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_REVISE');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_PLANNER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RUNNER');

-- ----------------------------
-- Table structure for tank
-- ----------------------------
DROP TABLE IF EXISTS `tank`;
CREATE TABLE `tank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_7gt0rkvf99ksoj1sfv5ibon7s` (`user_created_id`) USING BTREE,
  KEY `FK_19lquh9mmm6cqhbgmqjevd7ys` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_19lquh9mmm6cqhbgmqjevd7ys` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_7gt0rkvf99ksoj1sfv5ibon7s` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tank
-- ----------------------------

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_hnnrudu0mkqbreyh20uft82fd` (`user_created_id`) USING BTREE,
  KEY `FK_2tx26n9kdoa5k97kd2yd4h0tu` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_2tx26n9kdoa5k97kd2yd4h0tu` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_hnnrudu0mkqbreyh20uft82fd` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of unit
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_qleu8ddawkdltal07p8e6hgva` (`role_id`) USING BTREE,
  CONSTRAINT `FK_qleu8ddawkdltal07p8e6hgva` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com123', '1');
INSERT INTO `user` VALUES ('2', '', 'user', 'e10adc3949ba59abbe56e057f20f883e', 'Abulhamid@gmail.com', '1');
SET FOREIGN_KEY_CHECKS=1;
