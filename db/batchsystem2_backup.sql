/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 100131
Source Host           : localhost:3306
Source Database       : batchsystem2

Target Server Type    : MYSQL
Target Server Version : 100131
File Encoding         : 65001

Date: 2018-08-02 10:42:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for global_setting
-- ----------------------------
DROP TABLE IF EXISTS `global_setting`;
CREATE TABLE `global_setting` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `key_object` varchar(30) DEFAULT NULL,
  `value_object` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `UK_mbqytb0rb9amst48ynpmyejgx` (`key_object`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of global_setting
-- ----------------------------
INSERT INTO `global_setting` VALUES ('1', 'ip_hmi_webservice', '192.168.1.35:8089');

-- ----------------------------
-- Table structure for planning
-- ----------------------------
DROP TABLE IF EXISTS `planning`;
CREATE TABLE `planning` (
  `date` date NOT NULL,
  `plan_id` int(11) NOT NULL,
  `plan_no` varchar(100) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`date`,`plan_id`),
  KEY `FK_pddxgn51pq6wvc535g10a5uko` (`user_created_id`) USING BTREE,
  KEY `plan_id` (`plan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of planning
-- ----------------------------
INSERT INTO `planning` VALUES ('2018-05-09', '1', null, '2018-05-09 10:50:26', '1');
INSERT INTO `planning` VALUES ('2018-07-04', '0', null, '2018-07-19 14:42:35', '1');

-- ----------------------------
-- Table structure for planning_list
-- ----------------------------
DROP TABLE IF EXISTS `planning_list`;
CREATE TABLE `planning_list` (
  `plan_list_id` int(20) NOT NULL AUTO_INCREMENT,
  `plan_id` int(11) DEFAULT NULL,
  `batch_number` varchar(50) DEFAULT NULL,
  `batch_code` varchar(50) DEFAULT NULL,
  `qty_plan` int(100) DEFAULT NULL,
  `tank_id` bigint(20) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_running` bit(1) DEFAULT NULL,
  `recipe_id` bigint(20) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `user_created_id` bigint(20) DEFAULT NULL,
  `is_deleted` int(1) DEFAULT '0',
  PRIMARY KEY (`plan_list_id`),
  UNIQUE KEY `indexPlan` (`plan_list_id`),
  KEY `recipePlan` (`recipe_id`) USING BTREE,
  KEY `tankPlan` (`tank_id`) USING BTREE,
  KEY `FK_plan` (`plan_id`),
  CONSTRAINT `FK_plan` FOREIGN KEY (`plan_id`) REFERENCES `planning` (`plan_id`),
  CONSTRAINT `FK_recipe` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`),
  CONSTRAINT `FK_tank` FOREIGN KEY (`tank_id`) REFERENCES `tank` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planning_list
-- ----------------------------
INSERT INTO `planning_list` VALUES ('1', '0', '', '', '1000', '1', '2018-07-24 22:26:44', '2018-07-26 08:37:02', '', '25', '2018-07-26 08:37:02', '1', '0');
INSERT INTO `planning_list` VALUES ('2', '0', '', '', '1000', '2', '2018-07-24 20:40:58', '2018-07-25 17:56:28', '', '25', '2018-07-25 17:56:28', '1', '0');
INSERT INTO `planning_list` VALUES ('3', '0', '', '', '600', '3', '2018-07-24 20:40:58', '2018-07-25 17:58:04', '\0', '25', '2018-07-25 17:58:04', '1', '0');
INSERT INTO `planning_list` VALUES ('4', '0', '', '', '500', '3', '2018-07-24 20:40:58', '2018-07-26 08:34:48', '', '25', '2018-07-26 08:34:48', '1', '0');
INSERT INTO `planning_list` VALUES ('5', '0', '', '', null, null, '2018-07-24 20:40:58', null, '\0', '25', '2018-07-24 20:40:58', '1', '0');
INSERT INTO `planning_list` VALUES ('6', '0', '', '', null, null, '2018-07-24 20:40:58', null, '\0', '25', '2018-07-24 20:40:58', '1', '0');
INSERT INTO `planning_list` VALUES ('7', '0', '', '', null, null, '2018-07-24 20:45:53', null, '\0', '7', '2018-07-24 20:45:53', '1', '0');
INSERT INTO `planning_list` VALUES ('8', '0', '', '', null, null, '2018-07-24 20:45:53', null, '\0', '7', '2018-07-24 20:45:53', '1', '0');
INSERT INTO `planning_list` VALUES ('9', '0', '', '', null, null, '2018-07-24 20:45:53', null, '\0', '7', '2018-07-24 20:45:53', '1', '0');
INSERT INTO `planning_list` VALUES ('10', '0', '', '', null, null, '2018-07-24 20:45:53', null, '\0', '7', '2018-07-24 20:45:53', '1', '0');
INSERT INTO `planning_list` VALUES ('11', '0', '', '', null, null, '2018-07-24 20:45:53', null, '\0', '7', '2018-07-24 20:45:53', '1', '0');
INSERT INTO `planning_list` VALUES ('12', '0', '', '', null, null, '2018-07-24 20:45:53', null, '\0', '7', '2018-07-24 20:45:53', '1', '0');

-- ----------------------------
-- Table structure for process
-- ----------------------------
DROP TABLE IF EXISTS `process`;
CREATE TABLE `process` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `process_type` int(11) DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  `unit_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_85j0ex0uioglxjuyoiyniip02` (`user_created_id`) USING BTREE,
  KEY `FK_e0vtuymeqdnsdks05o1pgw1o0` (`user_updated_id`) USING BTREE,
  KEY `FK_4iob6pcenddivl9qjngpaurgq` (`unit_id`) USING BTREE,
  CONSTRAINT `FK_4iob6pcenddivl9qjngpaurgq` FOREIGN KEY (`unit_id`) REFERENCES `unit` (`id`),
  CONSTRAINT `FK_85j0ex0uioglxjuyoiyniip02` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_e0vtuymeqdnsdks05o1pgw1o0` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of process
-- ----------------------------
INSERT INTO `process` VALUES ('1', '', '2018-05-09 10:47:21', '2018-05-09 10:47:21', 'EAP', '0', 'EAP', '1', '1', '1');
INSERT INTO `process` VALUES ('2', '', '2018-05-09 10:47:59', '2018-05-09 10:47:59', 'IPAP', '1', 'IPAP', '1', '1', '1');
INSERT INTO `process` VALUES ('3', '', '2018-05-09 10:48:44', '2018-05-09 10:48:44', 'AGIT', '0', 'AGIT', '1', '1', '2');
INSERT INTO `process` VALUES ('4', null, null, null, 'MEKP', null, 'MEKP', null, null, '1');

-- ----------------------------
-- Table structure for recipe
-- ----------------------------
DROP TABLE IF EXISTS `recipe`;
CREATE TABLE `recipe` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `activated_date` datetime DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `obsoluted_date` datetime DEFAULT NULL,
  `quantity` float DEFAULT NULL,
  `rejected_date` datetime DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  `user_activated_id` bigint(20) DEFAULT NULL,
  `user_approved_id` bigint(20) DEFAULT NULL,
  `user_obsoluted_id` bigint(20) DEFAULT NULL,
  `user_rejected_id` bigint(20) DEFAULT NULL,
  `value` text,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_a5a3c8m9owqkw9phvjysgehns` (`user_created_id`) USING BTREE,
  KEY `FK_8w7b8i5tl9teggj5q93ys762g` (`user_updated_id`) USING BTREE,
  KEY `FK_ihomkn8we2ckko6tuyrbxi9m6` (`user_activated_id`) USING BTREE,
  KEY `FK_297delmsy2d0jw8tkmkq8pqy4` (`user_approved_id`) USING BTREE,
  KEY `FK_ihjidmtxb58vuikjc7hge5lvm` (`user_obsoluted_id`) USING BTREE,
  KEY `FK_9xyt3siao5okyagnypk69qv90` (`user_rejected_id`) USING BTREE,
  CONSTRAINT `FK_297delmsy2d0jw8tkmkq8pqy4` FOREIGN KEY (`user_approved_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_8w7b8i5tl9teggj5q93ys762g` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_9xyt3siao5okyagnypk69qv90` FOREIGN KEY (`user_rejected_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_a5a3c8m9owqkw9phvjysgehns` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ihjidmtxb58vuikjc7hge5lvm` FOREIGN KEY (`user_obsoluted_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_ihomkn8we2ckko6tuyrbxi9m6` FOREIGN KEY (`user_activated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of recipe
-- ----------------------------
INSERT INTO `recipe` VALUES ('1', '', '2018-05-09 10:49:40', '2018-05-09 10:50:09', '2018-05-09 10:50:09', '2018-05-09 10:49:50', 'aa', '2018-06-23 04:09:38', '1000', null, 'aa1', '1', '1', '1', '1', '1', '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('7', '', '2018-06-23 04:03:52', null, '2018-06-23 04:04:59', '2018-06-23 04:04:45', 'ab', null, '500', null, 'ab1', '1', '1', null, null, null, null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('13', null, null, null, '2018-06-23 07:41:09', '2018-06-23 07:40:51', 'ah', '2018-06-23 07:41:18', '1000', null, 'ah', '1', null, null, '1', '1', '1', null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('25', '', null, null, '2018-06-29 13:44:36', '2018-06-29 13:44:22', 'asd', null, '500', null, 'asd', '1', '1', null, '1', '1', null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('26', '', '2018-07-19 14:42:02', null, null, null, 'receipe pertama', null, '500', '2018-07-23 14:35:24', 'biceng 1', '1', '1', null, null, null, null, '1', '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('27', '', '2018-07-20 13:43:16', null, '2018-07-20 16:23:24', '2018-07-20 16:23:10', 'nafiul', null, '500', null, 'nma', '1', '1', null, '1', '1', null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('28', '', '2018-07-23 08:31:29', null, null, null, 'asdf', null, '500', '2018-07-23 14:35:09', 'adsf', '1', '1', null, null, null, null, '1', '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('29', '', '2018-07-23 08:33:52', null, null, '2018-07-23 14:35:56', 'sadfadsafasd', null, '2134', null, 'ad', '1', '1', null, null, '1', null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('31', '', '2018-07-23 20:29:16', null, null, null, 'asd', null, '23423', null, '34', '1', '1', null, null, null, null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');
INSERT INTO `recipe` VALUES ('32', '', '2018-07-23 20:31:04', null, null, null, 'asd', null, '23423', null, 'asdf', '1', '1', null, null, null, null, null, '{\"data\":[{\"id_proses\":1,\"name\":\"EAP\",\"value\":\"2\"},{\"id_proses\":2,\"name\":\"IPAP\",\"value\":\"2\"},{\"id_proses\":3,\"name\":\"AGIT\",\"value\":\"2\"},{\"id_proses\":4,\"name\":\"MEKP\",\"value\":\"2\"}]}');

-- ----------------------------
-- Table structure for report
-- ----------------------------
DROP TABLE IF EXISTS `report`;
CREATE TABLE `report` (
  `date` date NOT NULL,
  `plan_id` int(11) NOT NULL,
  `plan_list_id` int(20) NOT NULL,
  `tank_id` bigint(20) NOT NULL,
  `process_id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `time` time NOT NULL,
  `pv` double DEFAULT NULL,
  PRIMARY KEY (`date`,`plan_id`,`tank_id`,`process_id`,`recipe_id`,`time`),
  KEY `FK_r7juuxjl0bxdycmso5ck19oqk` (`process_id`,`recipe_id`) USING BTREE,
  CONSTRAINT `FK_r7juuxjl0bxdycmso5ck19oqk` FOREIGN KEY (`process_id`) REFERENCES `process` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of report
-- ----------------------------
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:04:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:05:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:06:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:07:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:08:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:09:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:10:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:11:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:12:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:13:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:14:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:15:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '13:16:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:01:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:02:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:03:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:04:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:05:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:06:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:07:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:08:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:09:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:10:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:11:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:12:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:13:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:14:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:15:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:16:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:17:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:20', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:22', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:18:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:11', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:12', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:13', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:14', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:15', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:16', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:17', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:18', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:19', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:19:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:23', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:24', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:25', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:26', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:27', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:28', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:30', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:31', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:32', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:33', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:34', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:35', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:36', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:38', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:39', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:40', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:41', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:42', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:43', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:44', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:45', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:46', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:47', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:48', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:49', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:50', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:51', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:52', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:53', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:54', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:55', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:56', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:57', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:58', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:31:59', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:00', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:01', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:02', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:03', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:04', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:05', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:06', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:07', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:08', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:09', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:10', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:21', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:29', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:32:37', '2');
INSERT INTO `report` VALUES ('2018-07-25', '0', '1', '1', '1', '25', '14:33:57', '2');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `description` varchar(140) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_7m3eqg8noqk187mewgv2e5oee` (`user_created_id`) USING BTREE,
  KEY `FK_brt29v4kxb5kg964semh4t2vo` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_7m3eqg8noqk187mewgv2e5oee` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_brt29v4kxb5kg964semh4t2vo` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '', '2018-04-27 10:25:03', '2018-04-27 10:25:03', 'Administrator', 'Admin', null, null);
INSERT INTO `role` VALUES ('2', null, null, null, 'Users', 'User', null, null);
INSERT INTO `role` VALUES ('3', null, null, null, 'Manager', 'Manager', null, null);

-- ----------------------------
-- Table structure for role_list_authority
-- ----------------------------
DROP TABLE IF EXISTS `role_list_authority`;
CREATE TABLE `role_list_authority` (
  `role_id` bigint(20) NOT NULL,
  `list_authority` varchar(255) DEFAULT NULL,
  KEY `FK_5cbj9hyan2tu0bggh8v3t7620` (`role_id`) USING BTREE,
  CONSTRAINT `FK_5cbj9hyan2tu0bggh8v3t7620` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of role_list_authority
-- ----------------------------
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_USER_MANAGER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_MASTER_DATA');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_CREATOR');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_APPROVER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_ACTIVATOR');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_OBSOLUTER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RECIPE_REVISE');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_PLANNER');
INSERT INTO `role_list_authority` VALUES ('1', 'ROLE_RUNNER');

-- ----------------------------
-- Table structure for step
-- ----------------------------
DROP TABLE IF EXISTS `step`;
CREATE TABLE `step` (
  `recipe_id` bigint(20) NOT NULL,
  `step_id` int(11) NOT NULL,
  PRIMARY KEY (`recipe_id`,`step_id`) USING BTREE,
  CONSTRAINT `FK_bclpmti9g9mco7cua4yvus0q0` FOREIGN KEY (`recipe_id`) REFERENCES `recipe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of step
-- ----------------------------
INSERT INTO `step` VALUES ('1', '1');
INSERT INTO `step` VALUES ('1', '2');

-- ----------------------------
-- Table structure for step_process
-- ----------------------------
DROP TABLE IF EXISTS `step_process`;
CREATE TABLE `step_process` (
  `process_id` bigint(20) NOT NULL,
  `recipe_id` bigint(20) NOT NULL,
  `step_id` int(11) NOT NULL,
  `next_step_condition` int(11) DEFAULT NULL,
  `next_step_condition_value` double DEFAULT NULL,
  `sequence` int(11) DEFAULT NULL,
  `sp` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`process_id`,`recipe_id`,`step_id`) USING BTREE,
  KEY `FK_gfmpti01svdfm65tk52mrl852` (`recipe_id`,`step_id`) USING BTREE,
  CONSTRAINT `FK_8o7lfd7o0sawpday2kj4iy0wo` FOREIGN KEY (`process_id`) REFERENCES `process` (`id`),
  CONSTRAINT `FK_gfmpti01svdfm65tk52mrl852` FOREIGN KEY (`recipe_id`, `step_id`) REFERENCES `step` (`recipe_id`, `step_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of step_process
-- ----------------------------
INSERT INTO `step_process` VALUES ('1', '1', '1', '1', '0', '0', '12.3');
INSERT INTO `step_process` VALUES ('2', '1', '1', '0', '0', '1', '1000');
INSERT INTO `step_process` VALUES ('3', '1', '2', '1', '0', '0', '65.7');

-- ----------------------------
-- Table structure for tank
-- ----------------------------
DROP TABLE IF EXISTS `tank`;
CREATE TABLE `tank` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT NULL,
  `updated_date` timestamp NULL DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_7gt0rkvf99ksoj1sfv5ibon7s` (`user_created_id`) USING BTREE,
  KEY `FK_19lquh9mmm6cqhbgmqjevd7ys` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_19lquh9mmm6cqhbgmqjevd7ys` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_7gt0rkvf99ksoj1sfv5ibon7s` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of tank
-- ----------------------------
INSERT INTO `tank` VALUES ('1', '', '2018-05-09 10:46:30', '2018-05-09 10:46:30', 'Tank 1', 'Tank1', '1', '1');
INSERT INTO `tank` VALUES ('2', null, null, null, 'Tank 2', 'Tank2', null, null);
INSERT INTO `tank` VALUES ('3', null, null, null, '', 'Tank3', null, null);
INSERT INTO `tank` VALUES ('4', null, null, null, '', 'Tank4', null, null);
INSERT INTO `tank` VALUES ('5', null, null, null, '', 'Tank5', null, null);
INSERT INTO `tank` VALUES ('6', null, null, null, '', 'Tank 6', null, null);
INSERT INTO `tank` VALUES ('7', null, null, null, 'Tank Position B', 'Tank7', null, null);
INSERT INTO `tank` VALUES ('8', null, null, null, 'Tank Mixer bagian A', 'Tank8', null, null);

-- ----------------------------
-- Table structure for unit
-- ----------------------------
DROP TABLE IF EXISTS `unit`;
CREATE TABLE `unit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `description` varchar(30) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `unique_code` varchar(20) DEFAULT NULL,
  `user_created_id` bigint(20) DEFAULT NULL,
  `user_updated_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_hnnrudu0mkqbreyh20uft82fd` (`user_created_id`) USING BTREE,
  KEY `FK_2tx26n9kdoa5k97kd2yd4h0tu` (`user_updated_id`) USING BTREE,
  CONSTRAINT `FK_2tx26n9kdoa5k97kd2yd4h0tu` FOREIGN KEY (`user_updated_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_hnnrudu0mkqbreyh20uft82fd` FOREIGN KEY (`user_created_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of unit
-- ----------------------------
INSERT INTO `unit` VALUES ('1', '', '2018-05-09 10:47:02', '2018-05-09 10:47:02', 'Kilo Gram', 'berat', 'kg', '1', '1');
INSERT INTO `unit` VALUES ('2', null, null, null, 'Liter', 'Cair', 'Ltr', null, null);
INSERT INTO `unit` VALUES ('3', null, null, null, 'Daftar Waktu', 'Waktu', 'dtk', null, null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `active` bit(1) DEFAULT NULL,
  `name` varchar(40) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `email` varchar(60) DEFAULT NULL,
  `role_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_qleu8ddawkdltal07p8e6hgva` (`role_id`) USING BTREE,
  CONSTRAINT `FK_qleu8ddawkdltal07p8e6hgva` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@gmail.com123', '1');
INSERT INTO `user` VALUES ('2', '', 'user', 'e10adc3949ba59abbe56e057f20f883e', 'Abulhamid@gmail.com', '1');
SET FOREIGN_KEY_CHECKS=1;
